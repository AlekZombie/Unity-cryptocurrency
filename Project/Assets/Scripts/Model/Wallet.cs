﻿using UnityEngine;

[CreateAssetMenu(fileName = "Wallet", menuName = "CryptoCurrency/Wallet File", order = 1)]
public class Wallet : ScriptableObject
{
    [Header("Wallet")]
    [SerializeField]
    private string _walletAddress; //of course your public wallet

    public string WalletAddress { get { return _walletAddress; } }
    
}
