﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;


[Serializable]
public class MoneroServer
{
    public RPCConfig config;

    #region RPC Retrieves
    /// <summary>
    /// 
    /// </summary>
    /// <param name="address">Wallet Address</param>
    /// <param name="amount">The amount to transfer on Atomic Units ( 1 XMR = 1e12, 0.01 = 1e9) </param>
    /// <param name="paymentId">The payment ID of the tx</param>
    /// <returns></returns>
    public string GetUri(string address, long amount, string paymentId)
    {
        var parameters = new Dictionary<string, System.Object> { { "address", address }, { "amount", amount }, { "payment_id", paymentId } };
        var info = GetData("make_uri", parameters);
        return !info.ContainsKey("uri") ? info["message"].ToString() : info["uri"].ToString();
    }

    public string GetTransferByTxId(string txId)
    {
        var parameters = new Dictionary<string, System.Object>() { { "txid", txId } };
        var info = GetData("get_transfer_by_txid", parameters);

        return JsonConvert.SerializeObject(info);
    }

    public Texture2D GetQrCode(string address, long amount, string paymentId)
    {
        var data = GetUri(address, amount, paymentId);
        if (data != null) return QR.GenerateQr(data);
        Debug.LogError("Problem with the server at the moment to retrieve the data");
        return null;
    }

    public Dictionary<string, System.Object> GetPayments(string paymentId)
    {
        var parameters = new Dictionary<string, System.Object>(){{"payment_id",paymentId}};
        var data = GetData("get_payments", parameters);
        return data.ContainsKey("payments") ? JsonConvert.DeserializeObject<Dictionary<string, System.Object>>(data["payments"].ToString()) : null;
    }
#endregion

    #region Server Private Methods

    private DigestHttpWebRequest GetRequest()
    {
        DigestHttpWebRequest request =
            new DigestHttpWebRequest(config.User, config.Pass)
            {
                Method = WebRequestMethods.Http.Post,
                ContentType = "application/json; charset=utf-8"
            };
        return request;
    }

    private Dictionary<string, System.Object> GetData(string methodName, Dictionary<string, System.Object> paramaters)
    {
        DigestHttpWebRequest request = GetRequest();

        Dictionary<string, System.Object> package = CreatingPackage(methodName, paramaters);

        SerializeAndSendingPackage(package, request);

        return GetDataFromResponse(request);
    }

    private void SerializeAndSendingPackage(Dictionary<string, object> package, DigestHttpWebRequest request)
    {
        string s = JsonConvert.SerializeObject(package);
        request.PostData = Encoding.UTF8.GetBytes(s);
    }

    private Dictionary<string, object> CreatingPackage(string methodName, ICollection paramaters)
    {
        Dictionary<string, System.Object> package =
            new Dictionary<string, System.Object> { { "jsonrpc", "2.0" }, { "id", "0" }, { "method", methodName } };
        if (paramaters.Count != 0)
        {
            package.Add("params", paramaters);
        }

        return package;
    }

    private Dictionary<string, System.Object> GetDataFromResponse(DigestHttpWebRequest request)
    {
        string data = string.Empty;
        WebResponse response = request.GetResponse(new Uri(string.Format("http://{0}:{1}/json_rpc",
            config.Domain, config.Port)));
        using (StreamReader reader = new StreamReader(response.GetResponseStream(), true))
        {
            data = reader.ReadToEnd();
        }

        var results = JsonConvert.DeserializeObject<Dictionary<string, System.Object>>(data);
        return JsonConvert.DeserializeObject<Dictionary<string, System.Object>>(results.ContainsKey("result") ? results["result"].ToString() : results["error"].ToString());
    }

    #endregion
}