﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

public class DigestHttpWebRequest
{
    private readonly string _user;
    private readonly string _password;
    private string _realm;
    private string _nonce;
    private string _qop;
    private string _cnonce;
    private Algorithm _md5;
    private DateTime _cnonceDate;
    private int _nc;

    private string _requestMethod = WebRequestMethods.Http.Get;

    public DigestHttpWebRequest(string user, string password)
    {
    _user = user;
    _password = password;
    }

    public string Method
    {
    get { return _requestMethod; }
    set { _requestMethod = value; }
    }

    public string ContentType { get; set; }

    public byte[] PostData { get; set; }

    public HttpWebResponse GetResponse(Uri uri, string data)
    {
        HttpWebResponse response = null;
        int infiniteLoopCounter = 0;
        int maxNumberAttempts = 2;

        while ((response == null ||
            response.StatusCode != HttpStatusCode.Accepted) &&
            infiniteLoopCounter < maxNumberAttempts)
        {
            var request = CreateHttpWebRequestObject(uri);

            // If we've got a recent Auth header, re-use it!
            if (!string.IsNullOrEmpty(_cnonce) &&
                DateTime.Now.Subtract(_cnonceDate).TotalHours < 1.0)
            {
                request.Headers.Add("Authorization", ComputeDigestHeader(uri));
            }

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }

            catch (WebException webException)
            {
                // Try to fix a 401 exception by adding a Authorization header
                if (webException.Response != null &&
                    ((HttpWebResponse)webException.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    var wwwAuthenticateHeader = webException.Response.Headers["WWW-Authenticate"];
                    _realm = GetDigestHeaderAttribute("realm", wwwAuthenticateHeader);
                    _nonce = GetDigestHeaderAttribute("nonce", wwwAuthenticateHeader);
                    _qop = GetDigestHeaderAttribute("qop", wwwAuthenticateHeader);
                    _md5 = GetMD5Algorithm(wwwAuthenticateHeader);

                    _nc = 0;
                    _cnonce = new Random().Next(123400, 9999999).ToString();
                    _cnonceDate = DateTime.Now;

                    request = CreateHttpWebRequestObject(uri, true);

                    infiniteLoopCounter++;
                    response = (HttpWebResponse)request.GetResponse();
                }
                else
                {
                    throw;
                }
            }

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                case HttpStatusCode.Accepted:
                    return response;
                case HttpStatusCode.Redirect:
                case HttpStatusCode.Moved:
                    uri = new Uri(response.Headers["Location"]);

                    // We decrement the loop counter, as there might be a variable number of redirections which we should follow
                    infiniteLoopCounter--;
                    break;
            }
        }

        throw new Exception("Error: Either authentication failed, authorization failed or the resource doesn't exist");
    }

    public HttpWebResponse GetResponse(Uri uri)
    {
        HttpWebResponse response = null;
        int infiniteLoopCounter = 0;
        int maxNumberAttempts = 2;

        while ((response == null ||
            response.StatusCode != HttpStatusCode.Accepted) &&
            infiniteLoopCounter < maxNumberAttempts)
        {
            var request = CreateHttpWebRequestObject(uri);

            // If we've got a recent Auth header, re-use it!
            if (!string.IsNullOrEmpty(_cnonce) &&
                DateTime.Now.Subtract(_cnonceDate).TotalHours < 1.0)
            {
                request.Headers.Add("Authorization", ComputeDigestHeader(uri));
            }

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }

            catch (WebException webException)
            {
                // Try to fix a 401 exception by adding a Authorization header
                if (webException.Response != null &&
                    ((HttpWebResponse)webException.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    var wwwAuthenticateHeader = webException.Response.Headers["WWW-Authenticate"];
                    _realm = GetDigestHeaderAttribute("realm", wwwAuthenticateHeader);
                    _nonce = GetDigestHeaderAttribute("nonce", wwwAuthenticateHeader);
                    _qop = GetDigestHeaderAttribute("qop", wwwAuthenticateHeader);
                    _md5 = GetMD5Algorithm(wwwAuthenticateHeader);

                    _nc = 0;
                    _cnonce = new Random().Next(123400, 9999999).ToString();
                    _cnonceDate = DateTime.Now;

                    request = CreateHttpWebRequestObject(uri, true);

                    infiniteLoopCounter++;
                    response = (HttpWebResponse)request.GetResponse();
                }
                else
                {
                    throw;
                }
            }

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                case HttpStatusCode.Accepted:
                    return response;
                case HttpStatusCode.Redirect:
                case HttpStatusCode.Moved:
                    uri = new Uri(response.Headers["Location"]);

                    // We decrement the loop counter, as there might be a variable number of redirections which we should follow
                    infiniteLoopCounter--;
                    break;
            }
        }

        throw new Exception("Error: Either authentication failed, authorization failed or the resource doesn't exist");
    }

    private HttpWebRequest CreateHttpWebRequestObject(Uri uri, bool addAuthenticationHeader)
    {
        var request = (HttpWebRequest)WebRequest.Create(uri);
        request.AllowAutoRedirect = false;
        request.Method = Method;

        if (!String.IsNullOrEmpty(ContentType))
        {
            request.ContentType = ContentType;
        }

        if (addAuthenticationHeader)
        {
            request.Headers.Add("Authorization", ComputeDigestHeader(uri));
        }

        if (PostData != null && PostData.Length > 0)
        {
            request.ContentLength = PostData.Length;
            Stream postDataStream = request.GetRequestStream(); //open connection
            postDataStream.Write(PostData, 0, PostData.Length); // Send the data.
            postDataStream.Close();
        }

        else if (
            Method == WebRequestMethods.Http.Post &&
            (PostData == null || PostData.Length == 0))
        {
            request.ContentLength = 0;
        }

        return request;
    }

    private HttpWebRequest CreateHttpWebRequestObject(Uri uri)
    {
        return CreateHttpWebRequestObject(uri, false);
    }

    private string ComputeDigestHeader(Uri uri)
    {
        _nc = _nc + 1;

        string ha1, ha2;

        switch (_md5)
        {
            case Algorithm.MD5sess:

                var secret = ComputeMd5Hash(string.Format(CultureInfo.InvariantCulture, "{0}:{1}:{2}", _user, _realm, _password));
                ha1 = ComputeMd5Hash(string.Format(CultureInfo.InvariantCulture, "{0}:{1}:{2}", secret, _nonce, _cnonce));
                ha2 = ComputeMd5Hash(string.Format(CultureInfo.InvariantCulture, "{0}:{1}", Method, uri.PathAndQuery));

                var data = string.Format(CultureInfo.InvariantCulture, "{0}:{1:00000000}:{2}:{3}:{4}",
                    _nonce,
                    _nc,
                    _cnonce,
                    _qop,
                    ha2);

                var kd = ComputeMd5Hash(string.Format(CultureInfo.InvariantCulture, "{0}:{1}", ha1, data));

                return string.Format("Digest username=\"{0}\", realm=\"{1}\", nonce=\"{2}\", uri=\"{3}\", " +
                    "algorithm=MD5-sess, response=\"{4}\", qop={5}, nc={6:00000000}, cnonce=\"{7}\"",
                    _user, _realm, _nonce, uri.PathAndQuery, kd, _qop, _nc, _cnonce);

            case Algorithm.MD5:

                ha1 = ComputeMd5Hash(string.Format("{0}:{1}:{2}", _user, _realm, _password));
                ha2 = ComputeMd5Hash(string.Format("{0}:{1}", Method, uri.PathAndQuery));
                var digestResponse =
                    ComputeMd5Hash(string.Format("{0}:{1}:{2:00000000}:{3}:{4}:{5}", ha1, _nonce, _nc, _cnonce, _qop, ha2));

                return string.Format("Digest username=\"{0}\", realm=\"{1}\", nonce=\"{2}\", uri=\"{3}\", " +
                    "algorithm=MD5, response=\"{4}\", qop={5}, nc={6:00000000}, cnonce=\"{7}\"",
                    _user, _realm, _nonce, uri.PathAndQuery, digestResponse, _qop, _nc, _cnonce);

        }

        throw new Exception("The digest header could not be generated");
    }

    private string GetDigestHeaderAttribute(string attributeName, string digestAuthHeader)
    {
        var regHeader = new Regex(string.Format(@"{0}=""([^""]*)""", attributeName));
        var matchHeader = regHeader.Match(digestAuthHeader);

        if (matchHeader.Success)
            return matchHeader.Groups[1].Value;

        throw new ApplicationException(string.Format("Header {0} not found", attributeName));
    }

    private Algorithm GetMD5Algorithm(string digestAuthHeader)
    {
        var md5Regex = new Regex(@"algorithm=(?<algo>.*)[,]", RegexOptions.IgnoreCase);
        var md5Attribute = md5Regex.Match(digestAuthHeader);

        if (md5Attribute.Success)
        {
            char[] charSeparator = { ',' };
            string algorithm = md5Attribute.Result("${algo}").ToLower().Split(charSeparator)[0];

            switch (algorithm)
            {
                case "md5-sess":
                case "\"md5-sess\"":
                    return Algorithm.MD5sess;
                default:
                    return Algorithm.MD5;
            }
        }

        throw new ApplicationException("Could not determine Digest algorithm to be used from the server response.");
    }



    private string ComputeMd5Hash(string input)
    {
        var inputBytes = Encoding.ASCII.GetBytes(input);
        var hash = MD5.Create().ComputeHash(inputBytes);
        var sb = new StringBuilder();

        foreach (var b in hash)
            sb.Append(b.ToString("x2"));
        return sb.ToString();
    }

    public enum Algorithm
    {
        MD5 = 0, // Apache Default
        MD5sess = 1 //IIS Default
    }
}